const baseConfig = {
  weddingDay: "Sunday",
  weddingTime: "06.00 PM- 09.00 PM",
  weddingDate: "Dec 12, 2021",
  showBuiltWithInfo: true,
  showQrCode: false,
  calendarInfo: {
    timeStartISO: "2021-12-12T19:00:00+08:00",
    timeEndISO: "2021-12-12T21:00:00+08:00",
  },
  coupleInfo: {
    brideName: "Abirami",
    groomName: "Prakash",
    coupleNameFormat: "GROOM_FIRST",
  },
  venue: {
    name: "Sri Amman Mahal",
    addressLine1: "Perumanallur, Tiruppur, NH-47",
    addressLine2: " Perumanallur Avinashi Coimbatore Road, Tiruppur, 638103",
    city: "Tiruppur, TamilNadu",
    country: "India",
    mapUrl: "https://goo.gl/maps/iWKwizEpKUyXT9cR7",
  },
  logo: {
    headerLogo: "/assets/images/ring-svg.png",
    footerLogo: "/assets/video/aw-ring-logo-ticker.mp4",
    footerLogoType: "mp4",
  },
  ogTags: {
    logo: "https://wedding-invitation-prakashabi.vercel.app/assets/images/cartoon.jpg",
    siteName: "wedding.wzulfikar.com",
    publishedTime: "2021-11-27",
  },
};

const lang = {
  id: {
    weddingDay: "Sabtu",
    weddingDate: "12 December 2021",
    venue: {
      ...baseConfig.venue,
      name: "Sri Amman Mahal",
      addressLine2: "Perumanallur, Tiruppur, 638103,",
    },
  },
};

export default {
  ...baseConfig,
  lang,
};
